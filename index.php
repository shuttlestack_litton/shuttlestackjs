<!doctype html>
<html lang="en" class="">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Shuttlestack builds high-quality applications and websites for startups and businesses and software for web developers." />
  <meta name="keywords" content="Shuttlestack, Web Designer, Mobile Designer, Software Development, Web Developers, Software, Code, New York City, Brooklyn, Web Agency" />
  <meta property="og:site_name" content="Shuttlestack" />
  <meta property="og:image" content="#" />
  <meta property="og:title" content="Shuttlestack - From Dream, To Reality" />
  <meta property="og:description" content="Shuttlestack builds high-quality applications and websites for startups and businesses and software for web developers." />
  <link rel="icon" href="img/favicon.png" type="image/x-icon" />
  <link rel="shortcut icon" href="public/images/favicon.png" type="image/x-icon" />
  <link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,600i,700,700i%7cPermanent+Marker" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <style type="text/css"> @media (max-width:629px) { #shownone { display: none; } } </style>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script>window.addEventListener("load", function(){ window.cookieconsent.initialise({ "palette": { "popup": { "background": "#ed3e44" }, "button": { "background": "#ffffff", "text": "#ed3e44" }}, "theme": "classic", "content": { "dismiss": "I Understand!", "link": "Privacy Policy", "href": "www.shuttlestack.com/cookies" }})});</script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132685864-1"></script>
  <script>window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-132685864-1');</script>
  <title>Shuttlestack - From Dream, To Reality</title>
</head>
<body>
  <div id="header"></div>
  <section class="height-80 bg-gradient-2">
      <img src="img/hero.jpg" alt="Image" class="bg-image opacity-20">
      <div class="container">
          <div class="row text-center">
              <div class="col">
                  <h1 class="display-3 text-white" style="font-weight: bold;">From Dream, To Reality.</h1>
                  <p class="lead text-white">Shuttlestack builds high-quality applications and websites for startups,
                      businesses, and web developers.</p> <a href="hire-us.html" class="btn btn-primary btn-lg" style="background: #fff; color: #ed3e44; border: solid 1px #fff; font-weight: bold;">Start
                  Your Journey</a>
              </div>
          </div>
      </div>
  </section>
  <section>
      <div class="container">
          <div class="row">
              <div class="col-12 col-md-6 d-md-flex">
                  <div class="card">
                      <div class="card-body d-flex py-4">
                          <div>
                              <h5 class="mb-1">Website Development</h5>
                              <p>Premium quality websites built with purpose for <a href="#" style="color: #ed3e44;">startups</a>,
                                  <a href="#" style="color: #ed3e44;">small businesses</a>, and <a href="#" style="color: #ed3e44;">non-profits</a>.
                              </p> <a href="#" style="color: #ed3e44;">Learn More</a>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-12 col-md-6 d-md-flex">
                  <div class="card">
                      <div class="card-body d-flex py-4">
                          <div>
                              <h5 class="mb-1">Mobile Development</h5>
                              <p>Modern mobile apps built with <a href="#" style="color: #ed3e44;">React Native</a>, <a href="#" style="color: #ed3e44;">Flutter</a>, <a href="#" style="color: #ed3e44;">Xamarin</a>,
                                  <a href="#" style="color: #ed3e44;">Swift</a>, and <a href="#" style="color: #ed3e44;">Java</a>.</p>
                                  <a href="#" style="color: #ed3e44;">Learn More</a>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-12 col-md-6 d-md-flex">
                  <div class="card">
                      <div class="card-body d-flex py-4">
                          <div>
                              <h5 class="mb-1">Software Development</h5>
                              <p><a href="#" style="color: #ed3e44;">Databases</a>, <a href="#" style="color: #ed3e44;">Software
                                  Developer Kits</a>, <a href="#" style="color: #ed3e44;">Application Programmable
                                  Interfaces</a>, and <a href="#" style="color: #ed3e44;">Desktop Applications</a>.</p> <a href="#" style="color: #ed3e44;">Learn More</a>

                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-12 col-md-6 d-md-flex">
                  <div class="card">
                      <div class="card-body d-flex py-4">
                          <div>
                              <h5 class="mb-1">Project Management</h5>
                              <p>Creative <a href="#" style="color: #ed3e44;">product roadmaps</a>, realistic <a href="#" style="color: #ed3e44;">deadlines</a>, and great-thinkers moving toward success.</p> <a href="#" style="color: #ed3e44;">Learn More</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <section class="row no-gutters p-0">
      <div class="col-md-5 col-lg-6 d-flex flex-column order-md-2">
          <img src="img/meeting.jpg" alt="Image" class="flex-grow-1">
      </div>
      <div class="col-md-7 col-lg-6 d-flex bg-white order-md-1 align-items-center">
          <div class="row no-gutters justify-content-center py-md-4">
              <div class="col-md-10 col-lg-9 col-xl-8 p-3 py-md-5">
                  <span class="h2 d-block">Group up with a team of trailblazers who actually care about your product.</span>
                  <p class="lead">Shuttlestack is a development company that invest in long-term relationships; not one-month contracts.</p> <a href="#" class="lead" style="color: #ed3e44;">Learn More</a>
              </div>
          </div>
      </div>
  </section>
  <!--
  <section>
      <div class="container">
          <div class="row">
              <div class="col-lg-8 d-flex">
                  <a href="" class="card bg-dark hover-effect justify-content-end flex-fill">
                      <img class="card-img flex-grow-1" src="https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/free-bootstrap-admin-dashboard-templates.jpg"/>
                  </a>
              </div>
              <div class="col-lg-4 d-flex">
                  <div class="card">
                      <div class="card-body">
                          <div class="mb-3 mt-2">
                              <span class="h6">You have an account!</span>
                          </div>
                          <div class="mb-sm-5">
                              <span class="h2">Shuttlestack gives you a dashboard to manage your project.</span>
                              <br/><br/>
                              <p>We've built a new feature where you can manage your project by uploading content, photos, view your analytics, message our team, and manage your billing all in one place.</p>
                          </div>
                      </div>
                      <div class="card-footer">
                          <div class="d-flex align-items-center justify-content-between"><a href="#">Learn More</a>
                              <i class="fa fa-arrow-right text-dark"></i>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section> -->
  <section class="p-0 bg-white">
      <img src="img/code.png" alt="Image" class="bg-image position-md-absolute">
      <div class="container-fluid p-0">
          <div class="row no-gutters">
              <div class="col-md-7 col-lg-6 col-xl-5">
                  <div class="card p-0 p-md-3 m-3">
                      <div class="card-body p-0 p-md-4">
                          <span class="h2 d-block">Products built with clean, reliable code.</span>
                          <p class="lead">Shuttlestack writes smart, clean and reliable code that ensures your product
                              will properly scale with no stress.</p> <a href="#" class="btn btn-primary btn-lg">Learn
                          More</a>

                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <section class="spacer-y-3" style="background: #ed3e44;">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col col-12 text-center">
                  <span class="d-block mb-3 h2 text-white">Would you like to work with us?</span> <a class="btn btn-lg" href="hire-us.html" style="background: #fff; color: #ed3e44;">Hire
                  Our Team</a>
              </div>
          </div>
      </div>
  </section>
  <div id="footer"></div>
  <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"> </script>
  <script src="js/include.js" type="text/javascript"></script>
</body>
</html>
